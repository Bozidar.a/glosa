<?php

namespace App\Models;

use App\Models\Bank;
use App\Models\Customer;
use App\Models\Package;
use App\Models\Note;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    protected $table = 'invoices';

    protected $fillable = ['number', 'date', 'bank_id', 'customer_id'];

    const RELATIONS = ['bank', 'customer', 'packages', "notes"];

    public function bank()
    {
    	return $this->belongsTo(Bank::class, 'bank_id');
    }

    public function customer()
    {
    	return $this->belongsTo(Customer::class, 'customer_id');
    }

    public function packages()
    {
    	return $this->belongsToMany(Package::class, 'invoice_packages', 'invoice_id', 'package_id' )->withPivot('quantity', 'discount');
    }

    public function notes()
    {
    	return $this->hasMany(Note::class, 'invoice_id' );
    }
}
