<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Note extends Model
{
    protected $table = 'invoice_notes';

    protected $fillable = ['description', 'flag'];
}
