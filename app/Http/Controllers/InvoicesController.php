<?php

namespace App\Http\Controllers;

class InvoicesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('invoices.index');
    }
}
