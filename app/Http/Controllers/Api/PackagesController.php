<?php

namespace App\Http\Controllers\Api;

use App\Models\Package;

use Illuminate\Http\Request;

use App\Exports\PackageExport;

use Maatwebsite\Excel\Facades\Excel;

use App\Http\Controllers\Controller;


class PackagesController extends Controller
{
    public function list(Request $request)
    {
    	$packages = Package::query();

        $packages->when($request->has('orderBy') && isset($request->orderBy), function($q) use($request) {

            $q->orderBy($request->orderBy['name'], $request->orderBy['value']);
        }); 

        $packages->when($request->has('pattern') && isset($request->pattern), function($q) use($request) {

            $q->where('name', 'LIKE', '%' . $request->pattern . '%');
        });

        if($request->has('getAll')) {

            return $packages = $packages->get();
        
        } else {

            $take = $request->has('take') ? $request->take : 50;

            $skip = $request->has('skip') ? $request->skip : 0;

            $packages = $packages->skip($skip)->take($take)->get();
        }

        return response()->json($packages);
    }

    public function store(Request $request)
    {
    	$package = Package::create($request->all());

    	return response()->json($package);
    }

    public function update(Package $package, Request $request)
    {
    	$package->update($request->all());

    	return response()->json($package);
    }

    public function remove(Package $package) 
    {
        $package->delete();

        return response()->json('Successfully Deleted');
    }

    public function export() 
    {
        return Excel::download(new PackageExport, 'packages.xlsx');
    }
}
