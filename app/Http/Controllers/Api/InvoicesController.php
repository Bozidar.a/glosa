<?php

namespace App\Http\Controllers\Api;

use DB;
use App;
use PDF;

use App\Models\Invoice;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use App\Exports\InvoicesExport;
use Maatwebsite\Excel\Facades\Excel;

use App\Http\Controllers\Controller;

class InvoicesController extends Controller
{
    public function list(Request $request)
    {
    	$invoices = Invoice::query();

    	if($request->has('with') && is_array($request->with)) {

            $relations = array_intersect($request->with, Invoice::RELATIONS);
            
            $invoices = $invoices->with($relations);
        } 

        $invoices->when($request->has('orderBy') && isset($request->orderBy), function($q) use($request) {

            $q->orderBy($request->orderBy['name'], $request->orderBy['value']);
        }); 

        $invoices->when($request->has('pattern') && isset($request->pattern), function($q) use($request) {

            $q->where('name', 'LIKE', '%' . $request->pattern . '%');
        });

        if($request->has('getAll')) {

            return $invoices = $invoices->get();
        
        } else {

            $take = $request->has('take') ? $request->take : 50;

            $skip = $request->has('skip') ? $request->skip : 0;

            $invoices = $invoices->skip($skip)->take($take)->get();
        }

        return response()->json($invoices);
    }

    public function store(Request $request)
    {
    	$invoice = Invoice::create($request->all());

        if($request->has('packages')) {

            foreach ($request->packages as $product) {

                DB::table('invoice_packages')->insert([

                    'invoice_id' => $invoice->id,

                    'package_id' => $product['id'],
                    
                    'discount' => array_key_exists('discount', $product) ? $product['discount'] : 0,

                    'quantity' => $product['pivot']['quantity']
                ]);
            }
        }

        if($request->has('notes')) {

            foreach ($request->notes as $note) {

                DB::table('invoice_notes')->insert([

                    'invoice_id' => $invoice->id,
                    
                    'description' => $note['description'],

                    'flag' => $note['flag']
                ]);
            }
        }

        $invoice->load(Invoice::RELATIONS);
        
    	return response()->json($invoice);
    }

    public function update(Invoice $invoice, Request $request)
    {
    	$invoice->update($request->all());

        if($request->has('packages')) {

            DB::table('invoice_packages')->where("invoice_id", $invoice->id)->delete();

            foreach ($request->packages as $product) {

                DB::table('invoice_packages')->insert([

                    'invoice_id' => $invoice->id,

                    'package_id' => $product['id'],
                    
                    'discount' => array_key_exists('discount', $product) ? $product['discount'] : 0,

                    'quantity' => $product['pivot']['quantity']
                ]);
            }
        }

        if($request->has('notes')) {
            $invoice->notes()->update([
                "description" => $request->notes[0]["description"],
                "flag" => $request->notes[0]["flag"]
            ]);
        }
        
        $invoice->load(Invoice::RELATIONS);
        
    	return response()->json($invoice);
    }

    public function remove(Invoice $invoice) 
    {   
        $invoice->packages()->delete();
        
        $invoice->notes()->delete();

        $invoice->delete();

        return response()->json('Successfully Deleted');
    }

    public function export() 
    {
        return Excel::download(new InvoicesExport, 'invoices.xlsx');
    }

    public function generatePDF(Request $request) 
    {
        $data = $request->content;

        $dompdf = new \Dompdf\Dompdf();

        $dompdf->set_option('enable_html5_parser', TRUE);

        $html = "<html><head>";

        $html .= '<style>body {font-family: DejaVu Sans; background-color: #FDFDFD}</style>';

        $html .= '<style> .text-center { text-align: center } p, h2 { margin: 0px; line-height: 1.25 } .logo-image { display: block; margin: 0px; width: 150px } .signature-image { display: block; width: 85%; margin: 80px 0px 0px 0px } .v-baseline { vertical-align: baseline; } .producs-table td, .producs-table th { border: 1px solid grey; padding: 5px; line-height: 1.25 } .font-12 { font-size: 12px }</style>';

        // $html .= "<link type='text/css' rel='stylesheet' href='" . \URL::to('/') . "/css/pdf.css' media='all'>";

        $html .= "</head><body>";

        $html .= "<div class='text-center'>";

        $html .= "<img class='logo-image' src='data:image/png;base64, " . base64_encode(file_get_contents('images/logo.png')) . "' alt='logo'>";

        $html .= "</div>";

        $html .= $data;

        $html .= "<div class='text-center'>";

        $html .= "<img class='signature-image' src='data:image/png;base64, " . base64_encode(file_get_contents('images/signature1.png')) . "' alt='signature'>";

        $html .= "</div>";

        $html .= "</body>";

        $dompdf->loadHtml($html);

        $dompdf->render();

        $dompdf->stream("file.pdf");
    }
}
