<?php

namespace App\Http\Controllers\Api;

use App\Models\Customer;

use Illuminate\Http\Request;

use App\Exports\CustomerExport;

use Maatwebsite\Excel\Facades\Excel;

use App\Http\Controllers\Controller;


class CustomersController extends Controller
{
    public function list(Request $request)
    {
    	$customers = Customer::query();

        $customers->when($request->has('orderBy') && isset($request->orderBy), function($q) use($request) {

            $q->orderBy($request->orderBy['name'], $request->orderBy['value']);
        }); 

        $customers->when($request->has('pattern') && isset($request->pattern), function($q) use($request) {

            $q->where('name', 'LIKE', '%' . $request->pattern . '%');
        });

        if($request->has('getAll')) {

            return $customers = $customers->get();
        
        } else {

            $take = $request->has('take') ? $request->take : 50;

            $skip = $request->has('skip') ? $request->skip : 0;

            $customers = $customers->skip($skip)->take($take)->get();
        }

        return response()->json($customers);
    }

    public function store(Request $request)
    {
    	$customer = customer::create($request->all());

    	return response()->json($customer);
    }

    public function update(Customer $customer, Request $request)
    {
    	$customer->update($request->all());

    	return response()->json($customer);
    }

    public function remove(Customer $customer) 
    {
        $customer->delete();

        return response()->json('Successfully Deleted');
    }

    public function export() 
    {
        return Excel::download(new CustomerExport, 'customers.xlsx');
    }
}
