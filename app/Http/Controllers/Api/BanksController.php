<?php

namespace App\Http\Controllers\Api;

use App\Models\Bank;

use Illuminate\Http\Request;

use App\Exports\BankExport;

use Maatwebsite\Excel\Facades\Excel;

use App\Http\Controllers\Controller;


class BanksController extends Controller
{
    public function list(Request $request)
    {
    	$banks = Bank::query();

        $banks->when($request->has('orderBy') && isset($request->orderBy), function($q) use($request) {

            $q->orderBy($request->orderBy['name'], $request->orderBy['value']);
        }); 

        $banks->when($request->has('pattern') && isset($request->pattern), function($q) use($request) {

            $q->where('name', 'LIKE', '%' . $request->pattern . '%');
        });

        if($request->has('getAll')) {

            return $banks = $banks->get();
        
        } else {

            $take = $request->has('take') ? $request->take : 50;

            $skip = $request->has('skip') ? $request->skip : 0;

            $banks = $banks->skip($skip)->take($take)->get();
        }

        return response()->json($banks);
    }

    public function store(Request $request)
    {
    	$bank = Bank::create($request->all());

    	return response()->json($bank);
    }

    public function update(Bank $bank, Request $request)
    {
    	$bank->update($request->all());

    	return response()->json($bank);
    }

    public function remove(Bank $bank) 
    {
        $bank->delete();

        return response()->json('Successfully Deleted');
    }

    public function export() 
    {
        return Excel::download(new BankExport, 'banks.xlsx');
    }
}
