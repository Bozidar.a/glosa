<?php

namespace App\Exports;

use App\Models\Bank;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class BankExport implements FromCollection, WithHeadings
{

    public function collection()
    {
        return Bank::select('id', 'name', 'account', 'edb', 'street', 'city', 'phone')->get();
    }

    public function headings(): array
    {
        return [
            'ID',
            'Name',
            'Account',
            'EDB',
            'Street',
            'City',
            'Phone'
        ];
    }
}
