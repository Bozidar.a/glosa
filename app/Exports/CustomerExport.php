<?php

namespace App\Exports;

use App\Models\Customer;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class CustomerExport implements FromCollection, WithHeadings
{

    public function collection()
    {
        return Customer::select('id', 'name', 'street', 'city', 'post_code')->get();
    }

    public function headings(): array
    {
        return [
            'ID',
            'Name',
            'Street',
            'City',
            'Post Code'
        ];
    }
}
