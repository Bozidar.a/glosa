<?php

namespace App\Exports;

use App\Models\Package;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class PackageExport implements FromCollection, WithHeadings
{

    public function collection()
    {
        return Package::select('id', 'name', 'price')->get();
    }

    public function headings(): array
    {
        return [
            'ID',
            'Name',
            'Price'
        ];
    }
}
