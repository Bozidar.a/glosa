<?php

namespace App\Exports;

use App\Models\Invoice;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class InvoicesExport implements FromCollection, WithHeadings
{

    public function collection()
    {
        return Invoice::with('bank', 'customer', 'packages')->get();
    }

    public function headings(): array
    {
        return [
            'ID',
            'Number',
            'Date'
        ];
    }
}
