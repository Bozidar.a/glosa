<?php

namespace Database\Seeders;

use DB;

use App\Models\User;
use App\Models\Package;
use App\Models\Bank;
use App\Models\Customer;
use App\Models\Invoice;

use Illuminate\Database\Seeder;

class DBSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Sase',
            'email' => 'sase@mail.com',
            'password' => bcrypt('sase12345')
        ]);

        for($x = 1; $x <= 100; $x++) {
            Package::create([
                'name' => 'Курс по германски јазик ' . $x,
                'price' => '4400'
            ]);
        }

        for($x = 1; $x <= 100; $x++) {
            Customer::create([
                'name' => 'АДС Метал Груп ' . $x,
                'street' => 'ул. 1675 бр. 24 Шуто Оризари',
                'city' => 'Скопје',
                'post_code' => '1000'
            ]);
        }

        Bank::create([
            'name' => 'Комерцијална банка - Скопје',
            'account' => '300000003567502',
            'edb' => '4044012507234',
            'street' => 'Ул. Ѓорче Петров бр. 34',
            'city' => 'Скопје',
            'phone' => '02 6138198'
        ]);

        for($x = 1; $x <= 100; $x++) {
            Invoice::create([
                'number' => 'Бр. 46/03-21/П' . $x,
                'date' => '2021-03-26',
                'customer_id' => $x,
                'bank_id' => 1,
            ]);
        }

        DB::table('invoice_packages')->insert([
            'quantity'      => 1,
            'invoice_id'    => 1,
            'package_id'    => 1
        ]);
    }
}
