
require('./bootstrap');

window.Vue = require('vue').default;

// Comment following lines in development to see error logs
Vue.config.devtools     = true

Vue.config.debug        = true

Vue.config.performance  = true

Vue.config.silent       = true


const files = require.context('./', true, /\.vue$/i); files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

// Modal
import VModal from 'vue-js-modal'

Vue.use(VModal, {

    dynamic: true,

    injectModalsContainer: true
})

// Moment date 
Vue.use(require('vue-moment'))

// Notification
import Toasted from 'vue-toasted'

let Options = {

	duration: 3000, 

	type: 'success',

	position: 'bottom-center'
}

Vue.use(Toasted, Options)

// Custom Select
import vSelect from 'vue-select'

Vue.component('v-select', vSelect)


import Packages from './components/packages/Index'

const packages = new Vue({ 

	el: '#packages',

	components: { Packages }
})


import Banks from './components/banks/Index'

const banks = new Vue({ 

	el: '#banks',

	components: { Banks }
})

import Customers from './components/customers/Index'

const customers = new Vue({ 

	el: '#customers',

	components: { Customers }
})

import Invoices from './components/invoices/Index'

const invoices = new Vue({ 

	el: '#invoices',

	components: { Invoices }
})