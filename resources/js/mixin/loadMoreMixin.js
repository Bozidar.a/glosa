export const loadMoreMixin = {

	data() {
		return {
			take: 50,
			skip: 0,
			isLoading: false,
			loading: true,
			load: true
		}
	},

	mounted() {
		this.$nextTick(() => {
			$('.overflow-wrapper').on('scroll', () => {
				if ($('.overflow-wrapper').scrollTop() + $('.overflow-wrapper').innerHeight() >= $('.overflow-wrapper')[0].scrollHeight) this.loadMore()
			})
		})
	},

	methods: {
		orderTable(collection, value) {
			this.reverse = !this.reverse
			if (this.reverse) {
				return _.orderBy(collection, value)
			} else {
				return _.orderBy(collection, value, 'desc')
			}
		}
	}
}