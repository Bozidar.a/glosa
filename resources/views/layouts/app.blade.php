<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <style>
        .antialiased {
            -webkit-font-smoothing:antialiased;
            -moz-osx-font-smoothing:grayscale;
        }
    </style>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body class="antialiased">
    <div id="app">

        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
                @auth
                    <span class="navbar-brand" style="padding-top: 0px !important">
                        <img src="/images/logo.png" alt="" style="max-height: 30px">
                    </span>
                @endauth
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-drop" 
                aria-controls="navbar-drop" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbar-drop">
                    @auth
                        <ul class="navbar-nav mr-auto">
                            <li class="nav-item">
                                <a class="nav-link h5 px-3 pb-0 m-0" href="{{ route('packages') }}">Products</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link h5 px-3 pb-0 m-0" href="{{ route('banks') }}">Banks</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link h5 px-3 pb-0 m-0" href="{{ route('customers') }}">Customers</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link h5 px-3 pb-0 m-0" href="{{ route('invoices') }}">Invoices</a>
                            </li>
                        </ul>
                    @endauth
                    <ul class="navbar-nav ml-auto">
                        @guest
                            @if (Route::has('login'))
                                <li class="nav-item">
                                    <a class="nav-link h5 px-3 pb-0 m-0" href="{{ route('login') }}">{{ __('Login') }}</a>
                                </li>
                            @endif
                            
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link h5 px-3 pb-0 m-0" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link h5 pb-0 m-0 dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }}
                                </a>

                                <div class="dropdown-menu p-0 dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item h5 p-3 m-0" href="{{ route('logout') }}"
                                       onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main>
            @yield('content')
        </main>
    </div>
</body>
</html>
