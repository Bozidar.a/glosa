<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {

    return redirect('/login');
});

Auth::routes();

Route::get('/packages', [App\Http\Controllers\PackagesController::class, 'index'])->name('packages');

Route::get('/customers', [App\Http\Controllers\CustomersController::class, 'index'])->name('customers');

Route::get('/banks', [App\Http\Controllers\BanksController::class, 'index'])->name('banks');

Route::get('/invoices', [App\Http\Controllers\InvoicesController::class, 'index'])->name('invoices');




Route::post('/export/packages', [App\Http\Controllers\Api\PackagesController::class, 'export']);

Route::post('/export/customers', [App\Http\Controllers\Api\CustomersController::class, 'export']);

Route::post('/export/banks', [App\Http\Controllers\Api\BanksController::class, 'export']);

Route::post('/export/invoices', [App\Http\Controllers\Api\InvoicesController::class, 'export']);


