<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::middleware('auth:api')->get('/user', function (Request $request) {
    
    return $request->user();
});


Route::any('/packages/list',                [App\Http\Controllers\Api\PackagesController::class, 'list']);

Route::post('/packages/store',              [App\Http\Controllers\Api\PackagesController::class, 'store']);

Route::put('/packages/{package}',           [App\Http\Controllers\Api\PackagesController::class, 'update']);

Route::post('/packages/delete/{package}',   [App\Http\Controllers\Api\PackagesController::class, 'remove']);


Route::any('/banks/list',                   [App\Http\Controllers\Api\BanksController::class, 'list']);

Route::post('/banks/store',                 [App\Http\Controllers\Api\BanksController::class, 'store']);

Route::put('/banks/{bank}',                 [App\Http\Controllers\Api\BanksController::class, 'update']);

Route::post('/banks/delete/{bank}',         [App\Http\Controllers\Api\BanksController::class, 'remove']);


Route::any('/customers/list',               [App\Http\Controllers\Api\CustomersController::class, 'list']);

Route::post('/customers/store',             [App\Http\Controllers\Api\CustomersController::class, 'store']);

Route::put('/customers/{customer}',         [App\Http\Controllers\Api\CustomersController::class, 'update']);

Route::post('/customers/delete/{customer}', [App\Http\Controllers\Api\CustomersController::class, 'remove']);


Route::any('/invoices/list',                [App\Http\Controllers\Api\InvoicesController::class, 'list']);

Route::post('/invoices/store',              [App\Http\Controllers\Api\InvoicesController::class, 'store']);

Route::put('/invoices/{invoice}',           [App\Http\Controllers\Api\InvoicesController::class, 'update']);

Route::post('/invoices/delete/{invoice}',   [App\Http\Controllers\Api\InvoicesController::class, 'remove']);


Route::post('/invoices/generatePDF',        [App\Http\Controllers\Api\InvoicesController::class, 'generatePDF']);